import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/vue-cicd/",
  plugins: [
    vue(),
    Components({
      resolvers: [AntDesignVueResolver()],
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  server: {
    port: 4200,
  },
  // build: {
  //   rollupOptions: {
  //     // https://rollupjs.org/guide/en/#outputmanualchunks
  //     output: {
  //       manualChunks: {
  //         'group-user': [
  //           './src/UserDetails',
  //           './src/UserDashboard',
  //           './src/UserProfileEdit',
  //         ],
  //       },
  //     },
  //   },
  // },
});
