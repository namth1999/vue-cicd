import { shallowMount, RouterLinkStub } from "@vue/test-utils";
import RouterL from "../../src/components/testStub/RouterL.vue";

describe("RouterL", () => {
  it("display link text", () => {
    const text = "Hello Vue 3";
    const wrapper = shallowMount(RouterL, {
      props: { text },
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });

    expect(wrapper.text()).toContain(text);
  });
});
