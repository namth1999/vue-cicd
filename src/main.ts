import "@/assets/styles/tailwind.css";
import { createApp, ref } from "vue";
import type { Ref } from "vue";
import App from "./App.vue";
import Keycloak from "keycloak-js";
import { mainConfig } from "@/config/mainConfig";
import type { Plugin } from "@vue/runtime-core";
import { createBEAxios } from "@/utils/axios";
import type { AxiosInstance } from "axios";
import { createPinia } from "pinia";
import { createR } from "@/router";
import Antd from "ant-design-vue";
import CountryFlag from "vue-country-flag-next";
import mitt from "mitt";

const initOptions = {
  url: "https://ec2-3-72-246-167.eu-central-1.compute.amazonaws.com:8443/auth",
  realm: "nodejs-keycloak-aws",
  clientId: "BoroBazar-fe",
};

const keycloak = Keycloak(initOptions);
const emitter = mitt();

const app = createApp(App);

app.config.globalProperties.$keycloak = keycloak;
const axios: Ref<AxiosInstance> = ref(createBEAxios(app));
const router: Plugin = createR(app);
app.use(CountryFlag);
app.use(router);
app.use(Antd);
app.use(createPinia());
app.provide("emitter", emitter);

keycloak
  .init({
    onLoad: "check-sso",
    checkLoginIframe: false,
  })
  .then(async (auth) => {
    axios.value = createBEAxios(app);

    app.provide(mainConfig.axiosProvide, axios);

    app.mount("#app");
  })
  .catch((e) => {
    console.log(e);
  });

//Token Refresh
setInterval(() => {
  if (keycloak.authenticated) {
    keycloak
      .updateToken(mainConfig.keycloak.minValidity)
      .then((refreshed) => {
        if (refreshed) {
          console.log("refreshed");
          axios.value = createBEAxios(app);
        } else {
          console.warn(
            "Token not refreshed, valid for " +
              Math.round(
                (keycloak.timeSkew ? keycloak.timeSkew : 0) +
                  (keycloak.tokenParsed && keycloak.tokenParsed.exp
                    ? keycloak.tokenParsed.exp
                    : 0) -
                  new Date().getTime() / 1000
              ) +
              " seconds"
          );
        }
      })
      .catch((e) => {
        console.error(e);
      });
  }
}, mainConfig.keycloak.timeout);
