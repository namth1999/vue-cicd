import { defineStore } from "pinia";
import type { ItemCart } from "@/models/home/models";
import type { CartState } from "@/models/store/models";
import Decimal from "decimal.js";

export const useCartStore = defineStore("cart", {
  state: () =>
    ({
      active: false,
      items: [],
      isEmpty: true,
      totalItems: 0,
      totalUniqueItems: 0,
      total: 0,
    } as CartState),

  actions: {
    close() {
      this.active = false;
    },
    open() {
      this.active = true;
    },
    addItemToCart(item: ItemCart, buyQuantity: number) {
      if (buyQuantity <= 0)
        throw new Error("cartQuantity can't be zero or less than zero");

      const existingItemIndex = this.items.findIndex(
        (existingItem) => existingItem.id === item.id
      );

      if (existingItemIndex > -1) {
        const newItems: ItemCart[] = [...this.items];
        let newQuantity = newItems[existingItemIndex].buyQuantity;
        if (!newQuantity) {
          throw new Error("buyQuantity can't be zero or less than zero");
        }
        newQuantity += buyQuantity;
        newItems[existingItemIndex].buyQuantity = newQuantity;
        this.items = newItems;
      } else {
        this.items = [...this.items, { ...item, buyQuantity }];
      }
    },
    removeItemFromCart(id: string, buyQuantity: number) {
      this.items = this.items.reduce((acc: ItemCart[], item) => {
        if (item.id === id) {
          if (!item.buyQuantity) {
            throw new Error("buyQuantity can't be zero or less than zero");
          }
          const newQuantity = item.buyQuantity - buyQuantity;

          return newQuantity > 0
            ? [...acc, { ...item, buyQuantity: newQuantity }]
            : [...acc];
        }
        return [...acc, item];
      }, []);
    },
    calculateTotal() {
      if (this.items.length > 0)
        this.total = this.items.reduce((total, item) => {
          if (!item.buyQuantity) {
            throw new Error("buyQuantity undefined");
          }
          const sum = new Decimal(total);
          const itemTotal = new Decimal(item.price)
            .times(item.buyQuantity)
            .toNumber();
          return sum.plus(itemTotal).toNumber();
        }, 0);
    },
    calculateTotalItems() {
      this.totalItems = this.items.reduce((sum, item) => {
        if (!item.buyQuantity) {
          throw new Error("buyQuantity undefined");
        }
        return sum + item.buyQuantity;
      }, 0);
    },
    calculateUniqueItems() {
      this.totalUniqueItems = this.items.length;
    },
    clearItemFromCart(id: string) {
      this.items = this.items.filter((existingItem) => existingItem.id !== id);
    },
  },

  getters: {
    getActive(state): boolean {
      return state.active;
    },
    getItemFromCart(state) {
      return (id: string) => state.items.find((item) => item.id === id);
    },
    isInCart(state): (id: string) => boolean {
      return (id: string) => !!state.items.find((item) => item.id === id);
    },
    isInStock(state) {
      return (id: string) => {
        const item = state.items.find((item) => item.id === id);
        if (item && item.buyQuantity)
          return item.buyQuantity < item.remainingStock;
        return false;
      };
    },
  },
});
