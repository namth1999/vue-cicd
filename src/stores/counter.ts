import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", {
  state: () => ({
    counter: 1,
    numberOfChanges: 0,
  }),

  actions: {
    incrementBy(value: number) {
      this.counter += value;
      this.numberOfChanges += 1;
    },
  },

  getters: {
    doubleCount(state) {
      return state.counter * 2;
    },
  },
});
