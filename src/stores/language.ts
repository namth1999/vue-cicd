import { defineStore } from "pinia";
import type { Language } from "@/models/layouts/models";

export const useLanguageStore = defineStore("language", {
  state: () => ({
    language: {
      id: "en",
      name: "English - EN",
      value: "us",
    },
  }),

  actions: {
    setLanguage(language: Language) {
      this.language = language;
    },
  },

  getters: {
    languageValue(state) {
      return state.language.value;
    },
  },
});
