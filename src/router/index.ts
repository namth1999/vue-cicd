import { createRouter, createWebHistory } from "vue-router";
import type { App } from "vue";
import HomeView from "../views/HomeView.vue";
import type Keycloak from "keycloak-js";

const createRoutes = () => [
  {
    path: "/",
    name: "home",
    component: HomeView,
    meta: {
      requiresAuth: false,
      roles: ["user", "admin"],
    },
  },
  {
    path: "/about",
    name: "about",
    meta: {
      requiresAuth: true,
      roles: ["user", "admin"],
    },
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AboutView.vue"),
  },
  {
    path: "/admin",
    name: "admin",
    meta: {
      requiresAuth: true,
      roles: ["admin"],
    },
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AdminView.vue"),
  },
  {
    path: "/search",
    name: "search",
    meta: {
      requiresAuth: false,
      roles: ["admin", "user"],
    },
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/SearchView.vue"),
  },
  {
    path: "/product/:id",
    name: "product",
    meta: {
      requiresAuth: false,
      roles: ["admin", "user"],
    },
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/ProductView.vue"),
  },
  {
    path: "/error",
    name: "error",
    meta: {
      requiresAuth: false,
      roles: ["user", "admin"],
    },
    props: true,
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/ErrorView.vue"),
  },
];

const roleMatched = (
  route_roles: string[],
  keyCloak: Keycloak.KeycloakInstance
) => {
  let hasRole = false;
  if (route_roles) {
    route_roles.map((item) => {
      if (keyCloak.hasResourceRole(item)) {
        hasRole = true;
      }
    });
  }
  return hasRole;
};

export const createR = (app: App) => {
  const router = createRouter({
    history: createWebHistory("/vue-cicd/"),
    routes: createRoutes(),
  });

  router.beforeEach(async (to, from, next) => {
    const keycloak: Keycloak.KeycloakInstance =
      app.config.globalProperties.$keycloak;
    if (!keycloak?.authenticated && to.meta.requiresAuth) {
      next({ name: "home" });
      // redirect the user to the login page
    } else {
      if (!roleMatched(to.meta.roles, keycloak) && to.meta.requiresAuth) {
        next({ name: "home" });
      } else {
        next();
      }
    }
  });
  return router;
};
