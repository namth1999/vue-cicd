// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `mainConfig.ts` with `mainConfig.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const mainConfig = {
  production: import.meta.env.MODE,
  apis: {
    address: {
      getPage: "address",
    },
    brand: {
      getPage: "brand",
    },
    category: {
      getPage: "category",
      getAll: "category/getAll",
    },
    childrenCategory: {
      getPage: "childrencategory",
    },
    dietary: {
      getPage: "dietary",
    },
    galleryProduct: {
      getPage: "galleryproduct",
    },
    order: {
      getPage: "order",
    },
    orderProduct: {
      getPage: "orderproduct",
    },
    product: {
      getPage: "product",
    },
    shop: {
      getPage: "shop",
    },
    tag: {
      getPage: "tag",
    },
    tagProduct: {
      getPage: "tagproduct",
    },
    public: {
      getAllCategory: "public/category/getAll",
      searchProducts: "public/product/search",
      getHomeBestProducts: "public/product/getHomeBestProducts",
      getProductById: "public/product/getProductById",
      getRelatedProduct: "public/product/getRelatedProducts",
    },
  },
  keycloak: {
    keycloakProvide: Symbol("Keycloak provide key"),
    redirectUri: import.meta.env.VITE_ROOT,
    timeout: 6000, //ms
    minValidity: 1, //min
  },
  axiosProvide: Symbol("axios provide key"),
  piniaProvide: Symbol("pinia provide key"),
};
