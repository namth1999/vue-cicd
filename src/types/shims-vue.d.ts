declare module "*.vue" {
  import { defineComponent } from "vue";
  const component: ReturnType<typeof defineComponent>;
  export default component;
}

import mitt from "mitt";
declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    emitter: mitt;
  }
}
