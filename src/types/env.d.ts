/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_ROOT: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
