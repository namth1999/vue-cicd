declare module "vue3-carousel" {
  import { DefineComponent } from "vue";

  export const Carousel: DefineComponent<any, any, any, any, any>;
  export const Slide: DefineComponent<any, any, any, any, any>;
  export const Navigation: DefineComponent<any, any, any, any, any>;
}
