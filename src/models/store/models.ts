import type { ItemCart } from "@/models/home/models";

export interface CartState {
  active: boolean;
  items: ItemCart[];
  isEmpty: boolean;
  totalItems: number;
  totalUniqueItems: number;
  total: number;
}
