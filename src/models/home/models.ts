export interface BundleCardTwo {
  image: string;
  bgColor: string;
  description: string;
  id: number;
  title: string;
  slug: string;
}

export interface Category {
  image_name: string;
  icon: string;
  id: string;
  name: string;
  slug: string;
}

export interface Product {
  id: string;
  slug: string;
  name: string;
  description: string;
  image_name: string;
  quantity: number;
  price: number;
  sale_price: number;
  unit: string;
  total_buy: number;
  best_seller: boolean;
  flash_sale: boolean;
  category_id: string;
  p_tags?: string;
}

export interface ItemCart {
  id: string;
  name: string;
  slug: string;
  unit: string;
  image_name: string;
  price: number;
  buyQuantity?: number;
  remainingStock: number;
}
