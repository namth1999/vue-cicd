export interface Language {
  id: string;
  name: string;
  value: string;
}

export interface Payment {
  id: number;
  path: string;
  image: string;
  name: string;
  width: number;
  height: number;
}

export interface Social {
  id: number;
  path: string;
  image: string;
  name: string;
  width: number;
  height: number;
}

export interface WidgetLink {
  id: number;
  widgetTitle: string;
  lists: LinkList[];
}

export interface LinkList {
  id: number;
  title: string;
  icon?: string;
  path: string;
}

export interface Banner {
  id: number;
  title: string;
  description: string;
  btnText?: string;
  btnUrl?: string;
  searchBox: boolean;
  image: { mobile: { url: string }; desktop: { url: string } };
}
