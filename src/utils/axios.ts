import axios from "axios";
import type { AxiosInstance } from "axios";
import type Keycloak from "keycloak-js";
import type { App } from "vue";

export const createBEAxios = (app: App): AxiosInstance => {
  const keycloak: Keycloak.KeycloakInstance =
    app.config.globalProperties.$keycloak;
  return axios.create({
    timeout: 1000,
    headers: { Authorization: `Bearer ${keycloak.token}` },
    baseURL: "https://ec2-3-72-246-167.eu-central-1.compute.amazonaws.com:3000",
  });
};
