import { LoadingOutlined } from "@ant-design/icons-vue";
import { h } from "vue";

export const indicator = h(LoadingOutlined, {
  style: {
    fontSize: "120px",
    color: "#ffffff",
  },
  spin: true,
});
