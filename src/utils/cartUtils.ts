import type { ItemCart, Product } from "@/models/home/models";

const isProduct = (item: Product | ItemCart): item is Product => {
  return (item as Product).description !== undefined;
};

export const handleAddClick = (cartStore: any, item: Product | ItemCart) => {
  isProduct(item)
    ? cartStore.addItemToCart(generateCartItem(item), 1)
    : cartStore.addItemToCart(item, 1);
};

export const handleRemoveClick = (cartStore: any, id: string) => {
  cartStore.removeItemFromCart(id, 1);
};
const generateCartItem = (item: Product): ItemCart => {
  const { id, name, slug, image_name, price, sale_price, quantity, unit } =
    item;
  return {
    id,
    name,
    slug,
    unit,
    image_name,
    remainingStock: quantity,
    price: sale_price ? sale_price : price,
  };
};
