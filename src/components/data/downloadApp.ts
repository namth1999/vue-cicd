export const downloadApp = {
  title: "Make your online shop easier with our mobile app",
  description:
    "BoroBazar makes online grocery shopping fast and easy. Get groceries delivered and order the best of seasonal farm fresh food.",
  appImage:
    "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/app-thumbnail.png",
  appButtons: [
    {
      id: 1,
      slug: "/#",
      altText: "button-app-store",
      appButton:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/app-store.png",
      buttonWidth: 170,
      buttonHeight: 56,
    },
    {
      id: 2,
      slug: "/#",
      altText: "button-play-store",
      appButton:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/play-store.png",
      buttonWidth: 170,
      buttonHeight: 56,
    },
  ],
};
