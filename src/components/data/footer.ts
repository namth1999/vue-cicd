export const footer = {
  widgets: [
    {
      id: 1,
      widgetTitle: "About Us",
      lists: [
        {
          id: 1,
          title: "About us",
          path: "/about",
        },
        {
          id: 2,
          title: "Contact us",
          path: "/contact-us",
        },
      ],
    },
    {
      id: 2,
      widgetTitle: "Our information",
      lists: [
        {
          id: 1,
          title: "Privacy policy update",
          path: "/privacy",
        },
        {
          id: 2,
          title: "Terms & conditions",
          path: "/terms",
        },
        {
          id: 3,
          title: "Return Policy",
          path: "/privacy",
        },
        {
          id: 4,
          title: "Site map",
          path: "/",
        },
      ],
    },
    {
      id: 3,
      widgetTitle: "Community",
      lists: [
        {
          id: 1,
          title: "Announcements",
          path: "/",
        },
        {
          id: 2,
          title: "Answer center",
          path: "/",
        },
        {
          id: 3,
          title: "Discussion boards",
          path: "/",
        },
        {
          id: 4,
          title: "Giving works",
          path: "/",
        },
      ],
    },
  ],
  payment: [
    {
      id: 1,
      path: "/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/payment/mastercard.svg",
      name: "payment-master-card",
      width: 34,
      height: 20,
    },
    {
      id: 2,
      path: "/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/payment/visa.svg",
      name: "payment-visa",
      width: 50,
      height: 20,
    },
    {
      id: 3,
      path: "/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/payment/paypal.svg",
      name: "payment-paypal",
      width: 76,
      height: 20,
    },
    {
      id: 4,
      path: "/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/payment/jcb.svg",
      name: "payment-jcb",
      width: 26,
      height: 20,
    },
    {
      id: 5,
      path: "/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/payment/skrill.svg",
      name: "payment-skrill",
      width: 39,
      height: 20,
    },
  ],
  social: [
    {
      id: 1,
      path: "https://www.facebook.com/redqinc/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/social/facebook.svg",
      name: "facebook",
      width: 20,
      height: 20,
    },
    {
      id: 2,
      path: "https://twitter.com/redqinc",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/social/twitter.svg",
      name: "twitter",
      width: 20,
      height: 20,
    },
    {
      id: 3,
      path: "https://www.instagram.com/redqinc/",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/social/instagram.svg",
      name: "instagram",
      width: 20,
      height: 20,
    },
    {
      id: 4,
      path: "https://www.youtube.com/channel/UCjld1tyVHRNy_pe3ROLiLhw",
      image:
        "https://s3.eu-central-1.amazonaws.com/cdn.babozar/images/social/youtube.svg",
      name: "youtube",
      width: 20,
      height: 20,
    },
  ],
};
